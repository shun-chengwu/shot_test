# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
	This repostory is built for testing SHOT descriptor on different 3D models. Including some useful tools to convert/show/etc. point cloud.

### How do I get set up? ###

* Dependencies  
	PCL 1.7  
* How to run tests  
	`mkdir build`  
	`cd build`  
	`cmake ..`  
	`make`  

### Contribution guidelines ###

* Writing tests  
	Shun-Cheng Wu

** Note that some of the useful tools are the tutorial from PCL website  

### Who do I talk to? ###

* Repo owner or admin  
	Shun-Cheng Wu  