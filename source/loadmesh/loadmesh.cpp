#include "pcl/PCL_tools.hpp"

int
main (int argc, char *argv[])
{
    //
    //!!!!:  Load clouds
    //
    pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh);
    PCLloader().mesh(argv[1], mesh);

    //
    //!!!!:  Visualization
    //
    pcl::visualization::PCLVisualizer viewer;
    viewer.addPolygonMesh(*mesh);
    
    viewer.setBackgroundColor(1, 1, 1); //set backgroun to white
    while (!viewer.wasStopped ())
    {
        viewer.spinOnce ();
    }
    
    return (0);
}
