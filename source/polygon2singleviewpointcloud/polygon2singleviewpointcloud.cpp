#include <pcl/io/pcd_io.h>
#include <pcl/io/obj_io.h>

#include <pcl/visualization/pcl_visualizer.h>

#include <vtkPLYReader.h>
#include <vtkOBJReader.h>
#include <vtkPolyDataMapper.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/console/parse.h>

#include "pcl/PCL_tools.hpp"

typedef pcl::PointXYZ PointType;
std::string model_filename_;
int counter = 0;
enum DataType{ply, obj} datatype;

void parseCommandLine (int argc, char *argv[]){
    std::vector<int> obj_file_indices, ply_file_indices;
    obj_file_indices = pcl::console::parse_file_extension_argument (argc, argv, ".obj");
    ply_file_indices = pcl::console::parse_file_extension_argument (argc, argv, ".ply");
    if (obj_file_indices.size () < 1 && ply_file_indices.size() <1)
    {
        std::cout << "Filenames missing.\n";
        exit (-1);
    }
    if(obj_file_indices.size()>ply_file_indices.size()){
        model_filename_ = argv[obj_file_indices[0]];
        datatype = obj;
    } else {
        model_filename_ = argv[ply_file_indices[0]];
        datatype = ply;
    }
}

void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event, void* viewer_void)
{
     boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *>(viewer_void);
    if (event.getKeySym() == "r" && event.keyDown()){
//        std::cout << "'r' was pressed" << std::endl;
        pcl::PointCloud<PointType>::Ptr cloud_tmp (new pcl::PointCloud<PointType> ());
        viewer->renderView(800, 600, cloud_tmp);
        pcl::PointCloud<PointType>::Ptr cloud(new pcl::PointCloud<PointType> ());
        for (std::vector<PointType, Eigen::aligned_allocator<PointType> >::iterator it = cloud_tmp->points.begin();
             it != cloud_tmp->points.end();++it){
//            std::cout<<cloud->size()<<std::endl;
            if(it->x == it->x) cloud->push_back(*it);
        }
        
        PCLUtils::centroid<PointType> centroid;
        centroid.initialise(cloud);
        centroid.find_centroid();
        //move centroid to zero
        centroid.move_centroid_to_origion(cloud);
        
        pcl::io::savePCDFile(model_filename_ + std::to_string(++counter) + ".pcd", *cloud);
        std::cout << "file saved at " << model_filename_ + std::to_string(counter) + ".pcd" << std::endl;
        pcl::visualization::PCLVisualizer::Ptr viewer2(new pcl::visualization::PCLVisualizer);
        viewer2->addPointCloud(cloud);
        while (!viewer2->wasStopped ())
        {
            viewer2->spinOnce ();
        }
    }
    //按下r就render PointCloud1並儲存，用另一個視窗顯示
}

int
main (int argc, char **argv)
{
    //!!!!:Parsing
    parseCommandLine(argc, argv);
    
    //!!!!: Load model
    vtkSmartPointer<vtkPolyData> polydata1;
    if (datatype == ply) {
        vtkSmartPointer<vtkPLYReader> readerQuery = vtkSmartPointer<vtkPLYReader>::New ();
        readerQuery->SetFileName (model_filename_.c_str());
        polydata1 = readerQuery->GetOutput ();
        readerQuery->Update();
    } else if (datatype == obj){
        vtkSmartPointer<vtkOBJReader> readerQuery = vtkSmartPointer<vtkOBJReader>::New ();
        readerQuery->SetFileName (model_filename_.c_str());
        polydata1 = readerQuery->GetOutput ();
        readerQuery->Update();
    }
    
    
    //!!!!:Viewer
    // Add keyboardEvent
    pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer);
    pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType> ());
    
    viewer->registerKeyboardCallback(keyboardEventOccurred, (void*)& viewer);
    viewer->addModelFromPolyData(polydata1, "mesh1", 0);
    // Using event to save point clouds (also numbering the name)
    while (!viewer->wasStopped ())
    {
        viewer->spinOnce ();
    }
    return 0;
}
