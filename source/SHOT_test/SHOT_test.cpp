#include <pcl/io/obj_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/correspondence.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/shot_omp.h>
#include <pcl/features/board.h>
#include <pcl/filters/uniform_sampling.h>
//Grouping
#include <pcl/recognition/cg/hough_3d.h>
#include <pcl/recognition/cg/geometric_consistency.h>

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/common/transforms.h>
#include <pcl/console/parse.h>

#include <pcl/PCL_tools.hpp>

typedef pcl::PointXYZRGBA PointType;
typedef pcl::Normal NormalType;
typedef pcl::ReferenceFrame RFType;
typedef pcl::SHOT352 DescriptorType;

std::string model_filename_;
std::string scene_filename_;
//enum DataType{pcd, obj} datatype;

//Algorithm params
bool show_original_points_ (false);
bool show_keypoints_ (false);
bool show_correspondences_ (false);
bool show_rot (false);
bool use_cloud_resolution_ (false);
bool filtering_outlier (false);
bool use_smoothing (false);
bool use_hough_ (true);
float model_ss_ (0.01f);
float scene_ss_ (0.03f);
float rf_rad_ (0.015f);
float descr_rad_ (0.02f);
float cg_size_ (0.01f);
float cg_thresh_ (5.0f);
int meanK_ (50);
float stddevMulThresh_ (3.0);
float smoothingradius_ (0.1);

void
showHelp (char *filename)
{
    std::cout << std::endl;
    std::cout << "***************************************************************************" << std::endl;
    std::cout << "*                                                                         *" << std::endl;
    std::cout << "*             Correspondence Grouping Tutorial - Usage Guide              *" << std::endl;
    std::cout << "*                                                                         *" << std::endl;
    std::cout << "***************************************************************************" << std::endl << std::endl;
    std::cout << "Usage: " << filename << " model_filename.pcd scene_filename.pcd [Options]" << std::endl << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "     -h:                     Show this help." << std::endl;
    std::cout << "     -o:                     Show original poitns." << std::endl;
    std::cout << "     -k:                     Show used keypoints." << std::endl;
    std::cout << "     -c:                     Show used correspondences." << std::endl;
    std::cout << "     -r:                     Compute the model cloud resolution and multiply" << std::endl;
    std::cout << "                             each radius given by that value." << std::endl;
    std::cout << "     -f:                     Remove outlier of  model cloud with parameter meanK & stdMT" << std::endl;
    std::cout << "     -s:                     Smoothing model cloud with parameter meanK & stdMT" << std::endl;
    std::cout << "     -rot:                   Show rotated model position between two inputs" << std::endl;
    std::cout << "     --algorithm (Hough|GC): Clustering algorithm used (default Hough)." << std::endl;
    std::cout << "     --model_ss val:         Model uniform sampling radius (default 0.01)" << std::endl;
    std::cout << "     --scene_ss val:         Scene uniform sampling radius (default 0.03)" << std::endl;
    std::cout << "     --rf_rad val:           Reference frame radius (default 0.015)" << std::endl;
    std::cout << "     --descr_rad val:        Descriptor radius (default 0.02)" << std::endl;
    std::cout << "     --cg_size val:          Cluster size (default 0.01)" << std::endl;
    std::cout << "     --cg_thresh val:        Clustering threshold (default 5)" << std::endl;
    std::cout << "     --stdMT val:            stdandard dev Mul threshold (default 3)" << std::endl;
    std::cout << "     --meanK val:            set meanK of standard outlier removal (default 50)" << std::endl;
    std::cout << "     --smoothing_rad val:    Set the raidus of smoothing (default 0.1)" << std::endl;
    std::cout << std::endl;
}

void
parseCommandLine (int argc, char *argv[])
{
    //Show help
    if (pcl::console::find_switch (argc, argv, "-h"))
    {
        showHelp (argv[0]);
        exit (0);
    }
    
    //Model & scene filenames
    std::vector<int> filenames_tmp, filenames;
    filenames_tmp = pcl::console::parse_file_extension_argument (argc, argv, ".obj");
    for (int i=0;i<filenames_tmp.size();++i) filenames.push_back(filenames_tmp[i]);
    filenames_tmp = pcl::console::parse_file_extension_argument (argc, argv, ".pcd");
    for (int i=0;i<filenames_tmp.size();++i) filenames.push_back(filenames_tmp[i]);
    filenames_tmp = pcl::console::parse_file_extension_argument (argc, argv, ".ply");
    for (int i=0;i<filenames_tmp.size();++i) filenames.push_back(filenames_tmp[i]);
    
    if (filenames.size () < 2)
    {
        std::cout << "Filenames missing.\n";
        showHelp (argv[0]);
        exit (-1);
    } else if (filenames.size() > 2) {
        std::cout << "Too many input files. Should be two. \n";
        showHelp (argv[0]);
        exit (-1);
    }
    model_filename_ = argv[filenames[0]];
    scene_filename_ = argv[filenames[1]];

    
    //Program behavior
    if (pcl::console::find_switch (argc, argv, "-o"))
    {
        show_original_points_ = true;
    }
    if (pcl::console::find_switch (argc, argv, "-k"))
    {
        show_keypoints_ = true;
    }
    if (pcl::console::find_switch (argc, argv, "-c"))
    {
        show_correspondences_ = true;
    }
    if (pcl::console::find_switch (argc, argv, "-r"))
    {
        use_cloud_resolution_ = true;
    }
    if (pcl::console::find_switch (argc, argv, "-f"))
    {
        filtering_outlier = true;
    }
    if (pcl::console::find_switch (argc, argv, "-s"))
    {
        use_smoothing = true;
    }
    if (pcl::console::find_switch (argc, argv, "-rot"))
    {
        show_rot = true;
    }
    
    std::string used_algorithm;
    if (pcl::console::parse_argument (argc, argv, "--algorithm", used_algorithm) != -1)
    {
        if (used_algorithm.compare ("Hough") == 0)
        {
            use_hough_ = true;
        }else if (used_algorithm.compare ("GC") == 0)
        {
            use_hough_ = false;
        }
        else
        {
            std::cout << "Wrong algorithm name.\n";
            showHelp (argv[0]);
            exit (-1);
        }
    }
    
    //General parameters
    pcl::console::parse_argument (argc, argv, "--model_ss", model_ss_);
    pcl::console::parse_argument (argc, argv, "--scene_ss", scene_ss_);
    pcl::console::parse_argument (argc, argv, "--rf_rad", rf_rad_);
    pcl::console::parse_argument (argc, argv, "--descr_rad", descr_rad_);
    pcl::console::parse_argument (argc, argv, "--cg_size", cg_size_);
    pcl::console::parse_argument (argc, argv, "--cg_thresh", cg_thresh_);
    pcl::console::parse_argument (argc, argv, "--meanK", meanK_);
    pcl::console::parse_argument (argc, argv, "--stdMT", stddevMulThresh_);
    pcl::console::parse_argument (argc, argv, "--smoothing_rad", smoothingradius_);
    
    std::cout<<"--------------Parameters--------------"<<std::endl;
    std::cout<<"show_original_points:        "<< show_original_points_ << std::endl;
    std::cout<<"show_keypoints_:        "<< show_keypoints_ << std::endl;
    std::cout<<"show_correspondences_:  " <<show_correspondences_ << std::endl;
    std::cout<<"show_rot:               " <<show_rot << std::endl;
    std::cout<<"use_cloud_resolution_:  "<< use_cloud_resolution_ << std::endl;
    std::cout<<"filtering_outlier:      " <<filtering_outlier << std::endl;
    std::cout<<"use_smoothing:          " << use_smoothing << std::endl;
    std::cout<<"use_hough:              " << use_hough_ <<std::endl;
    std::cout<<"show_keypoints:         "<<show_keypoints_<<std::endl;
    std::cout<<"show_correspondences:   "<<show_correspondences_<<std::endl;
    std::cout<<"use_cloud_resolution:   "<<use_cloud_resolution_<<std::endl;
    std::cout<<"use_hough:              "<<use_hough_<<std::endl;
    std::cout<<"model_ss:               "<<model_ss_<<std::endl;
    std::cout<<"scene_ss:               "<<scene_ss_<<std::endl;
    std::cout<<"descr_rad:              "<<descr_rad_<<std::endl;
    std::cout<<"rf_rad:                 "<<rf_rad_<<std::endl;
    std::cout<<"cg_size:                "<<cg_size_<<std::endl;
    std::cout<<"cg_thresh:              "<<cg_thresh_<<std::endl;
    std::cout<<"meanK:                  "<<meanK_<<std::endl;
    std::cout<<"stdMT:                  "<<stddevMulThresh_<<std::endl;
    std::cout<<"smoothing_rad:          "<<smoothingradius_<<std::endl;
    std::cout<<"--------------------------------------"<<std::endl;
}

void compute_normal (pcl::NormalEstimationOMP<PointType, NormalType>& norm_est, pcl::PointCloud<NormalType>::Ptr model_normals, pcl::PointCloud<NormalType>::Ptr scene_normals, pcl::PointCloud<PointType>::Ptr model, pcl::PointCloud<PointType>::Ptr scene){
    norm_est.setKSearch (10);
    norm_est.setInputCloud (model);
    norm_est.compute (*model_normals);
    
    norm_est.setInputCloud (scene);
    norm_est.compute (*scene_normals);
}

void downsample_cloud (pcl::UniformSampling<PointType>& uniform_sampling, pcl::PointCloud<PointType>::Ptr model, pcl::PointCloud<PointType>::Ptr scene, pcl::PointCloud<PointType>::Ptr model_keypoints, pcl::PointCloud<PointType>::Ptr scene_keypoints){
    uniform_sampling.setInputCloud (model);//
    uniform_sampling.setRadiusSearch (model_ss_);//搜尋半徑
    uniform_sampling.filter (*model_keypoints);//找keypoint
    
    uniform_sampling.setInputCloud (scene);
    uniform_sampling.setRadiusSearch (scene_ss_);
    uniform_sampling.filter (*scene_keypoints);
}

void compute_descriptor_for_keypoints (pcl::SHOTEstimationOMP<PointType, NormalType, DescriptorType>& descr_est, pcl::PointCloud<DescriptorType>::Ptr model_descriptors, pcl::PointCloud<DescriptorType>::Ptr scene_descriptors, pcl::PointCloud<PointType>::Ptr model, pcl::PointCloud<PointType>::Ptr scene, pcl::PointCloud<PointType>::Ptr model_keypoints, pcl::PointCloud<PointType>::Ptr scene_keypoints, pcl::PointCloud<NormalType>::Ptr model_normals, pcl::PointCloud<NormalType>::Ptr scene_normals){
    descr_est.setRadiusSearch (descr_rad_);
    
    descr_est.setInputCloud (model_keypoints);
    descr_est.setInputNormals (model_normals);
    descr_est.setSearchSurface (model);
    descr_est.compute (*model_descriptors);
    
    descr_est.setInputCloud (scene_keypoints);
    descr_est.setInputNormals (scene_normals);
    descr_est.setSearchSurface (scene);
    descr_est.compute (*scene_descriptors);
}

void find_model_scene_correspondence (pcl::KdTreeFLANN<DescriptorType>& match_search, pcl::CorrespondencesPtr model_scene_corrs, pcl::PointCloud<DescriptorType>::Ptr scene_descriptors, pcl::PointCloud<DescriptorType>::Ptr model_descriptors){
    match_search.setInputCloud (model_descriptors);
    for (size_t i = 0; i < scene_descriptors->size (); ++i)
    {
        std::vector<int> neigh_indices (1);
        std::vector<float> neigh_sqr_dists (1);
        if (!pcl_isfinite (scene_descriptors->at (i).descriptor[0])) //skipping NaNs
        {
            continue;
        }
        int found_neighs = match_search.nearestKSearch (scene_descriptors->at (i), 1, neigh_indices, neigh_sqr_dists);
        if(found_neighs == 1 && neigh_sqr_dists[0] < 0.25f) //  add match only if the squared descriptor distance is less than 0.25 (SHOT descriptor distances are between 0 and 1 by design)                  scene_descriptors[i]
        {
            pcl::Correspondence corr (neigh_indices[0], static_cast<int> (i), neigh_sqr_dists[0]);
            model_scene_corrs->push_back (corr);
        }
    }
}

void actual_clustering (std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> >& rototranslations, std::vector<pcl::Correspondences>& clustered_corrs, pcl::CorrespondencesPtr& model_scene_corrs, pcl::PointCloud<PointType>::Ptr model, pcl::PointCloud<PointType>::Ptr scene, pcl::PointCloud<PointType>::Ptr model_keypoints, pcl::PointCloud<PointType>::Ptr scene_keypoints, pcl::PointCloud<NormalType>::Ptr model_normals, pcl::PointCloud<NormalType>::Ptr scene_normals){
    
    //  Using Hough3D
    if (use_hough_)
    {
        //
        //  Compute (Keypoints) Reference Frames only for Hough
        //
        std::cout<<"Hough Algorithm is used"<<std::endl;
        pcl::PointCloud<RFType>::Ptr model_rf (new pcl::PointCloud<RFType> ());
        pcl::PointCloud<RFType>::Ptr scene_rf (new pcl::PointCloud<RFType> ());
        
        pcl::BOARDLocalReferenceFrameEstimation<PointType, NormalType, RFType> rf_est;
        rf_est.setFindHoles (true);
        rf_est.setRadiusSearch (rf_rad_);
        
        rf_est.setInputCloud (model_keypoints);
        rf_est.setInputNormals (model_normals);
        rf_est.setSearchSurface (model);
        rf_est.compute (*model_rf);
        
        rf_est.setInputCloud (scene_keypoints);
        rf_est.setInputNormals (scene_normals);
        rf_est.setSearchSurface (scene);
        rf_est.compute (*scene_rf);
        
        //  Clustering
        pcl::Hough3DGrouping<PointType, PointType, RFType, RFType> clusterer;
        clusterer.setHoughBinSize (cg_size_);
        clusterer.setHoughThreshold (cg_thresh_);
        clusterer.setUseInterpolation (true);
        clusterer.setUseDistanceWeight (false);
        
        clusterer.setInputCloud (model_keypoints);
        clusterer.setInputRf (model_rf);
        clusterer.setSceneCloud (scene_keypoints);
        clusterer.setSceneRf (scene_rf);
        clusterer.setModelSceneCorrespondences (model_scene_corrs);
        
        //clusterer.cluster (clustered_corrs);
        clusterer.recognize (rototranslations, clustered_corrs);
    }
    else // Using GeometricConsistency
    {
        std::cout<<"GC Algorithm is used"<<std::endl;
        pcl::GeometricConsistencyGrouping<PointType, PointType> gc_clusterer;
        gc_clusterer.setGCSize (cg_size_);
        gc_clusterer.setGCThreshold (cg_thresh_);
        
        gc_clusterer.setInputCloud (model_keypoints);
        gc_clusterer.setSceneCloud (scene_keypoints);
        gc_clusterer.setModelSceneCorrespondences (model_scene_corrs);
        
        //gc_clusterer.cluster (clustered_corrs);
        gc_clusterer.recognize (rototranslations, clustered_corrs);
    }
}


int counter=0;
void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event, void* viewer_void)
{
    typedef pcl::PointXYZ PointType;
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *>(viewer_void);
    if (event.getKeySym() == "r" && event.keyDown()){
        //        std::cout << "'r' was pressed" << std::endl;
        pcl::PointCloud<PointType>::Ptr cloud_tmp (new pcl::PointCloud<PointType> ());
        viewer->renderView(800, 600, cloud_tmp);
        pcl::PointCloud<PointType>::Ptr cloud(new pcl::PointCloud<PointType> ());
        for (std::vector<PointType, Eigen::aligned_allocator<PointType> >::iterator it = cloud_tmp->points.begin();
             it != cloud_tmp->points.end();++it){
            //            std::cout<<cloud->size()<<std::endl;
            if(it->x == it->x) cloud->push_back(*it);
        }
        

        PCLUtils::centroid<PointType> centroid;
        centroid.initialise(cloud);
        centroid.find_centroid();
        //move centroid to zero
        centroid.move_centroid_to_origion(cloud);
        
        pcl::io::savePCDFile(model_filename_ + std::to_string(++counter) + ".pcd", *cloud);
        pcl::io::savePLYFile(model_filename_ + std::to_string(counter) + ".ply", *cloud);
//        pcl::io::savePLYFile(model_filename_ + std::to_string(counter) + ".ply"), cloud);
        std::cout << "file saved at " << model_filename_ + std::to_string(counter) + ".pcd" << std::endl;
        std::cout << "file saved at " << model_filename_ + std::to_string(counter) + ".ply" << std::endl;
        pcl::visualization::PCLVisualizer::Ptr viewer2(new pcl::visualization::PCLVisualizer);
        viewer2->addPointCloud(cloud);
        while (!viewer2->wasStopped ())
        {
            viewer2->spinOnce ();
        }
    }
    //按下r就render PointCloud1並儲存，用另一個視窗顯示
}



int
main (int argc, char *argv[])
{
    //Show parsed arguments
//    std::cout<<"Argument parsed:"<<std::endl;
//    for(size_t i=0;i<argc;++i){
//        std::cout<<"argument["<<i<<"]= "<<argv[i]<<std::endl;
//    }
    parseCommandLine (argc, argv);
    pcl::PointCloud<PointType>::Ptr model (new pcl::PointCloud<PointType> ());
    pcl::PointCloud<PointType>::Ptr model_keypoints (new pcl::PointCloud<PointType> ());
    pcl::PointCloud<PointType>::Ptr scene (new pcl::PointCloud<PointType> ());
    pcl::PointCloud<PointType>::Ptr scene_keypoints (new pcl::PointCloud<PointType> ());
    pcl::PointCloud<NormalType>::Ptr model_normals (new pcl::PointCloud<NormalType> ());
    pcl::PointCloud<NormalType>::Ptr scene_normals (new pcl::PointCloud<NormalType> ());
    pcl::PointCloud<DescriptorType>::Ptr model_descriptors (new pcl::PointCloud<DescriptorType> ());
    pcl::PointCloud<DescriptorType>::Ptr scene_descriptors (new pcl::PointCloud<DescriptorType> ());
    
    //
    //!!!!:  Load clouds
    //
    PCLloader().cloud<PointType>(model_filename_, model);
    PCLloader().cloud<PointType>(scene_filename_, scene);
    
    //Move to origion:
    PCLUtils::centroid<PointType> cen1, cen2;
    cen1.initialise(model);
    cen1.find_centroid();
    cen1.move_centroid_to_origion(model);
    cen2.initialise(scene);
    cen2.find_centroid();
    cen2.move_centroid_to_origion(scene);
    
    //
    //!!!!:  Preprocessing clouds
    //
    //Filtering outliers
    if(filtering_outlier) model = PCLUtils::filteringobject<PointType>(model, meanK_, stddevMulThresh_);
    
    //Smoothing
    if(use_smoothing) model = PCLUtils::smoothing<PointType> (model, true, smoothingradius_);
    

    
    //
    //!!!!:  Set up resolution invariance
    //
    if (use_cloud_resolution_)
    {
//        PCLUtils::computeCloudResolution<PointType>(model);
        float resolution = static_cast<float> (PCLUtils::computeCloudResolution<PointType>(model));
        if (resolution != 0.0f)
        {
            model_ss_   *= resolution;
            scene_ss_   *= resolution;
            rf_rad_     *= resolution;
            descr_rad_  *= resolution;
            cg_size_    *= resolution;
        }
        
        std::cout << "Model resolution:       " << resolution << std::endl;
        std::cout << "Model sampling size:    " << model_ss_ << std::endl;
        std::cout << "Scene sampling size:    " << scene_ss_ << std::endl;
        std::cout << "LRF support radius:  	   " << rf_rad_ << std::endl;
        std::cout << "SHOT descriptor radius: " << descr_rad_ << std::endl;
        std::cout << "Clustering bin size:    " << cg_size_ << std::endl << std::endl;
    }
    
    //
    //!!!!:  Compute Normals
    //
    pcl::NormalEstimationOMP<PointType, NormalType> norm_est;
    compute_normal (norm_est, model_normals, scene_normals, model, scene);
    
    //
    //!!!!:  Downsample Clouds to Extract keypoints
    //
    pcl::UniformSampling<PointType> uniform_sampling;
    downsample_cloud (uniform_sampling, model, scene, model_keypoints, scene_keypoints);
    std::cout << "Model total points: " << model->size () << "; Selected Keypoints: " << model_keypoints->size () << std::endl;
    std::cout << "Scene total points: " << scene->size () << "; Selected Keypoints: " << scene_keypoints->size () << std::endl;
    
    //
    //!!!!:  Compute Descriptor for keypoints
    //
    pcl::SHOTEstimationOMP<PointType, NormalType, DescriptorType> descr_est;
    compute_descriptor_for_keypoints (descr_est, model_descriptors, scene_descriptors, model, scene, model_keypoints, scene_keypoints, model_normals, scene_normals);
    
    //
    //!!!!:  Find Model-Scene Correspondences with KdTree
    //
    pcl::CorrespondencesPtr model_scene_corrs (new pcl::Correspondences ());
    pcl::KdTreeFLANN<DescriptorType> match_search;
    find_model_scene_correspondence (match_search, model_scene_corrs, scene_descriptors, model_descriptors);
    std::cout << "Correspondences found: " << model_scene_corrs->size () << std::endl;
    
    //
    //!!!!:  Actual Clustering
    //
    std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > rototranslations;
    std::vector<pcl::Correspondences> clustered_corrs;
    actual_clustering(rototranslations, clustered_corrs, model_scene_corrs, model, scene, model_keypoints, scene_keypoints, model_normals, scene_normals);
    
    //
    //!!!!:  Output results
    //
    std::cout << "Model instances found: " << rototranslations.size () << std::endl;
    for (size_t i = 0; i < rototranslations.size (); ++i)
    {
        std::cout << "\n    Instance " << i + 1 << ":" << std::endl;
        std::cout << "        Correspondences belonging to this instance: " << clustered_corrs[i].size () << std::endl;
        
        // Print the rotation matrix and translation vector
        Eigen::Matrix3f rotation = rototranslations[i].block<3,3>(0, 0);
        Eigen::Vector3f translation = rototranslations[i].block<3,1>(0, 3);
        //        std::cout<<rototranslations[i]<<std::endl;
        printf ("\n");
        printf ("            | %6.3f %6.3f %6.3f | \n", rotation (0,0), rotation (0,1), rotation (0,2));
        printf ("        R = | %6.3f %6.3f %6.3f | \n", rotation (1,0), rotation (1,1), rotation (1,2));
        printf ("            | %6.3f %6.3f %6.3f | \n", rotation (2,0), rotation (2,1), rotation (2,2));
        printf ("\n");
        printf ("        t = < %0.3f, %0.3f, %0.3f >\n", translation (0), translation (1), translation (2));
    }
    
    //
    //!!!!:  Visualization
    //
    
    //SHOW ORIGINAL POINTS
    //Change General points colour
    PCLUtils::pointwisecolorhandler<PointType>(model,100, 250);
    PCLUtils::pointwisecolorhandler<PointType>(scene,100, 250);
    pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer);
    if(show_original_points_) viewer->addPointCloud (scene, "scene_cloud");
    
    pcl::PointCloud<PointType>::Ptr off_scene_model (new pcl::PointCloud<PointType> ());
    pcl::PointCloud<PointType>::Ptr off_scene_model_keypoints (new pcl::PointCloud<PointType> ());
    
    if (show_correspondences_ || show_keypoints_)
    {
        //  We are translating the model so that it doesn't end in the middle of the scene representation
        pcl::transformPointCloud (*model, *off_scene_model, Eigen::Vector3f (-1,0,0), Eigen::Quaternionf (1, 0, 0, 0));
        pcl::transformPointCloud (*model_keypoints, *off_scene_model_keypoints, Eigen::Vector3f (-1,0,0), Eigen::Quaternionf (1, 0, 0, 0));
        
//        pcl::visualization::PointCloudColorHandlerCustom<PointType> off_scene_model_color_handler (off_scene_model, 1, 1, 1);
        if(show_original_points_) viewer->addPointCloud (off_scene_model, "off_scene_model");
    }
    
    if (show_keypoints_)
    {
        //Change keypoints colour
        PCLUtils::pointwisecolorhandler<PointType>(scene_keypoints, 0, 255, 0, 100);
        PCLUtils::pointwisecolorhandler<PointType>(off_scene_model_keypoints, 0, 255, 0, 100);
        
        viewer->addPointCloud (scene_keypoints, "scene_keypoints");
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "scene_keypoints");

        viewer->addPointCloud (off_scene_model_keypoints, "off_scene_model_keypoints");
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "off_scene_model_keypoints");
    }
    //Set Keypoint colour
//    PCLUtils::VitruvianManifold<PointType>(off_scene_model_keypoints);
    
    //
    pcl::PointCloud<PointType>::Ptr corres_model (new pcl::PointCloud<PointType> ());
    pcl::PointCloud<PointType>::Ptr corres_scene (new pcl::PointCloud<PointType> ());
    for (size_t i = 0; i < rototranslations.size (); ++i)
    {
        pcl::PointCloud<PointType>::Ptr rotated_model (new pcl::PointCloud<PointType> ());
        pcl::transformPointCloud (*model, *rotated_model, rototranslations[i]);

        std::stringstream ss_cloud;
        ss_cloud << "instance" << i;
        
        PCLUtils::pointwisecolorhandler<PointType>(rotated_model, 255, 0, 0, 15);
       if(show_rot) viewer->addPointCloud (rotated_model, ss_cloud.str ());

        if (show_correspondences_)
        {
            for (size_t j = 0; j < clustered_corrs[i].size (); ++j)
            {
                std::stringstream ss_line;
                ss_line << "correspondence_line" << i << "_" << j;
                PointType& model_point = off_scene_model_keypoints->at (clustered_corrs[i][j].index_query);
                PointType& scene_point = scene_keypoints->at (clustered_corrs[i][j].index_match);
//                scene_point.rgba = model_point.rgba;
                corres_model->points.push_back(model_point);
                corres_scene->points.push_back(scene_point);
                //  We are drawing a line for each pair of clustered correspondences found between the model and the scene
                viewer->addLine<PointType, PointType> (model_point, scene_point, 0, 255, 0, ss_line.str ());
            }
            
        }
    }
    if (show_correspondences_){
        PCLUtils::VitruvianManifold<PointType>(corres_model);
        PCLUtils::pointwisecolorhandler<PointType>(corres_model, corres_scene);
        viewer->addPointCloud(corres_model, "corres_model");
        viewer->addPointCloud(corres_scene, "corres_scene");
    }
    viewer->registerKeyboardCallback(keyboardEventOccurred, (void*)& viewer);
//    viewer->setBackgroundColor(1, 1, 1); //set backgroun to white
    while (!viewer->wasStopped ())
    {
        viewer->spinOnce ();
    }
    
    return (0);
}
