#include "pcl/PCL_tools.hpp"
#include <pcl/common/transforms.h>
#include <pcl/simulation/range_likelihood.h>
#include <pcl/console/parse.h>
typedef pcl::PointXYZ PointType;

void
parseCommandLine (int argc, char *argv[])
{
//    std::cout << argc << std::endl;
    assert(argc == 3 && "");

}



int
main (int argc, char *argv[])
{
    parseCommandLine(argc, argv);
    //
    //!!!!:  Load clouds
    //
    pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType> ());
    PCLloader().cloud<PointType>(argv[1], cloud);
    
    pcl::PolygonMesh mesh;
    PCLUtils::pointcloud2polygonmesh<pcl::PointXYZ>(cloud, mesh, 50, 0.1, 10);
    
    
    //!!!!:  Visualization
    //SHOW ORIGINAL POINTS
    pcl::visualization::PCLVisualizer viewer ("Correspondence Grouping");
    
    viewer.setBackgroundColor(0, 1, 1); //set backgroun to white
    while (!viewer.wasStopped ())
    {
        viewer.spinOnce ();
    }
    
    //SHOW POLYGON MESH
    pcl::visualization::PCLVisualizer viewer2 ("Polygon VIEW");
    viewer2.addPolygonMesh(mesh);
    while (!viewer2.wasStopped ())
    {
        viewer2.spinOnce ();
    }
    
    //SAVE
    pcl::io::saveOBJFile(argv[2], mesh);
    
    return (0);
}


