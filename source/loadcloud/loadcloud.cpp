#include "pcl/PCL_tools.hpp"

int
main (int argc, char *argv[])
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>());
    PCLloader().cloud<pcl::PointXYZ>(argv[1], cloud);

    //
    //!!!!:  Visualization
    //
    //SHOW ORIGINAL POINTS
    pcl::visualization::PCLVisualizer viewer ("Correspondence Grouping");
    viewer.addPointCloud(cloud);
    
    viewer.setBackgroundColor(0, 0, 0); //set backgroun to white
    while (!viewer.wasStopped ())
    {
        viewer.spinOnce ();
    }
    
    return (0);
}
