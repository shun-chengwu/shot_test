#include "pcl/PCL_tools.hpp"

#include <pcl/common/transforms.h>
#include <pcl/simulation/range_likelihood.h>
#include <pcl/console/parse.h>
typedef pcl::PointXYZ PointType;
std::string input_name;
std::string output_name;

void
parseCommandLine (int argc, char *argv[])
{
//    std::cout << argc << std::endl;
    assert(argc == 3 && "");
    //Model & scene filenames
    input_name  = argv[1];
    output_name = argv[2];
}



int
main (int argc, char *argv[])
{
    parseCommandLine(argc, argv);
    //
    //!!!!:  Load clouds
    //
    pcl::PointCloud<PointType>::Ptr model (new pcl::PointCloud<PointType> ());
    PCLloader().cloud<PointType>(input_name, model);

    model = PCLUtils::filteringobject<PointType>(model, 1000, 5);
    
    
    //!!!!:  Visualization
    //SHOW ORIGINAL POINTS
    pcl::visualization::PCLVisualizer viewer ("Correspondence Grouping");

    viewer.addPointCloud(model);
    viewer.setBackgroundColor(0, 1, 1); //set backgroun to white
    while (!viewer.wasStopped ())
    {
        viewer.spinOnce ();
    }
  
    //SAVE
    pcl::io::savePCDFile(output_name + ".pcd", *model);
    
    return (0);
}

