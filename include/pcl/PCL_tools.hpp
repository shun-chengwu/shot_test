//
//  PCL_tools.hpp
//  SHOT_test
//
//  Created by Shun-Cheng Wu on 05/11/2017.
//
//
#ifndef PCL_tools_hpp
#define PCL_tools_hpp

#include <pcl/io/pcd_io.h>
#include <pcl/io/obj_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>

//Filter
#include <pcl/filters/statistical_outlier_removal.h>

//Smoothing
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>


#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/ml/kmeans.h>

class PCLloader {
public:
    PCLloader() {};
    void mesh (std::string path, pcl::PolygonMesh::Ptr mesh) {
        enum types {
            obj, ply
        } type;
        //obj, pcd, ply
        switch (path[path.size()-1]) {
            case 'j':
                type = obj;
                break;
            case 'y':
                type = ply;
                break;
            default:
//                throw "intput type doesn't support";
                std::cout<< "input type doesn't support" <<std::endl;
                exit(0);
        }
        switch (type) {
            case obj:
                if (pcl::io::loadOBJFile(path, *mesh) < 0){
                    std::cout<<"Error loading model cloud."<<std::endl;
                    exit(0);
                }
                break;
            case ply:
                if (pcl::io::loadPLYFile(path, *mesh) < 0){
                    std::cout <<"Error loading model cloud."<<std::endl;
                    exit(0);
                }
                break;
        }
    }
    
    template <typename T>
    void cloud (std::string path, typename pcl::PointCloud<T>::Ptr cloud) {
        enum types {
            pcd, obj, ply
        } type;
        //obj, pcd, ply
        switch (path[path.size()-1]) {
            case 'd':
                type = pcd;
                break;
            case 'j':
                type = obj;
                break;
            case 'y':
                type = ply;
                break;
            default:
                throw "intput type doesn't support";
        }
        switch (type) {
            case pcd:
                if (pcl::io::loadPCDFile(path, *cloud) < 0){
                    std::cout<<"Error loading model cloud."<<std::endl;
                    exit(0);
                }
                break;
            case obj:
                if (pcl::io::loadOBJFile(path, *cloud) < 0){
                    std::cout<< "Error loading model cloud."<<std::endl;
                    exit(0);
                }
                break;
            case ply:
                if (pcl::io::loadPLYFile(path, *cloud) < 0){
                    std::cout<<"Error loading model cloud."<<std::endl;
                    exit(0);
                }
                break;
        }
    }
};


namespace PCLUtils{
    
    

    
    
    template <typename T>
    double computeCloudResolution (const typename pcl::PointCloud<T>::ConstPtr &cloud){
        double res = 0.0;
        int n_points = 0;
        int nres;
        std::vector<int> indices (2);
        std::vector<float> sqr_distances (2);
        pcl::search::KdTree<T> tree;
        tree.setInputCloud (cloud);
        
        for (size_t i = 0; i < cloud->size (); ++i)
        {
            if (! pcl_isfinite ((*cloud)[i].x))
            {
                continue;
            }
            //Considering the second neighbor since the first is the point itself.
            nres = tree.nearestKSearch ((int)i, 2, indices, sqr_distances);
            if (nres == 2)
            {
                res += sqrt (sqr_distances[1]);
                ++n_points;
            }
        }
        if (n_points != 0)
        {
            res /= n_points;
        }
        return res;
    }
    
    
    template <typename T>
    void pointcloud2polygonmesh(typename pcl::PointCloud<T>::Ptr pointcloud, pcl::PolygonMesh& mesh, int Ksearch=20, float search_rad=0.025, float mu=10){
        // Normal estimation*
        pcl::NormalEstimation<T, pcl::Normal> n;
        pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
        typename pcl::search::KdTree<T>::Ptr tree (new pcl::search::KdTree<T>);
        tree->setInputCloud (pointcloud);
        n.setInputCloud (pointcloud);
        n.setSearchMethod (tree);
        n.setKSearch (Ksearch);
        n.compute (*normals);
        
        // Concatenate the XYZ and normal fields*
        pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
        pcl::concatenateFields (*pointcloud, *normals, *cloud_with_normals);
        //* cloud_with_normals = pointcloud + normals
        
        // Create search tree*
        pcl::search::KdTree<pcl::PointNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointNormal>);
        tree2->setInputCloud (cloud_with_normals);
        
        // Initialize objects
        pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
        
        // Set the maximum distance between connected points (maximum edge length)
        gp3.setSearchRadius (search_rad);
        
        // Set typical values for the parameters
        gp3.setMu (mu);
        gp3.setMaximumNearestNeighbors (100);
        gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
        gp3.setMinimumAngle(M_PI/18); // 5 degrees
        gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
        gp3.setNormalConsistency(false);
        
        // Get result
        gp3.setInputCloud (cloud_with_normals);
        gp3.setSearchMethod (tree2);
        gp3.reconstruct (mesh);
        
        // Additional vertex information
        std::vector<int> parts = gp3.getPartIDs();
        std::vector<int> states = gp3.getPointStates();
    }

    template <typename T>
    typename pcl::PointCloud<T>::Ptr filteringobject (typename pcl::PointCloud<T>::Ptr cloud,int meank, int stddevmulthresh){
        typename pcl::PointCloud<T>::Ptr cloud_filtered (new pcl::PointCloud<T>);;
        pcl::StatisticalOutlierRemoval<T> sor;
        sor.setInputCloud (cloud);
        sor.setMeanK (meank);
        sor.setStddevMulThresh (stddevmulthresh);
        sor.filter (*cloud_filtered);
        return cloud_filtered;
    }
    
    
    
    template <typename T>
    typename pcl::PointCloud<T>::Ptr smoothing (typename pcl::PointCloud<T>::Ptr cloud, bool PolynomialFit=true, float searchradius=0.03){
        typename pcl::MovingLeastSquares<T, pcl::PointNormal> mls;
        typename pcl::search::KdTree<T>::Ptr tree (new pcl::search::KdTree<T>);
        typename pcl::PointCloud<pcl::PointNormal> mls_points;
        typename pcl::PointCloud<T>::Ptr mls_cloud (new pcl::PointCloud<T>);
        mls.setComputeNormals (true);
        mls.setInputCloud (cloud);
        mls.setPolynomialFit (PolynomialFit);
        mls.setSearchMethod (tree);
        mls.setSearchRadius (searchradius);
        mls.process (mls_points);
        
        for (size_t i = 0; i < mls_points.points.size(); ++i)
        {
            const pcl::PointNormal &mls_pt = mls_points.points[i];
//            pcl::PointXYZ pt(mls_pt.x, mls_pt.y, mls_pt.z);
            T pt;
            pt.x = mls_pt.x;
            pt.y = mls_pt.y;
            pt.z = mls_pt.z;
            mls_cloud->push_back(pt);
        }
        return mls_cloud;
    }
    
    
    
    template <typename T>
    struct centroid {
        std::vector<float> x, y, z;
//        typename pcl::PointCloud<T>::Ptr cloud;
        typename boost::shared_ptr<pcl::PointCloud<T>> cloud;
        float cx, cy, cz; //centriod
        struct max { //max&min
            float x_max, y_max, z_max, x_min, y_min, z_min, xt, yt, zt, tt;
        } max;
        int size, nan=0;
        float x_len, y_len, z_len;
        void initialise(typename pcl::PointCloud<T>::Ptr input){
            cloud = input;
            size = cloud->size();
            x.resize(size);
            y.resize(size);
            z.resize(size);
            for (size_t i=0; i<size;++i){
                if(cloud->points[i].x == cloud->points[i].x){
                    x[i]=cloud->points[i].x;
                    y[i]=cloud->points[i].y;
                    z[i]=cloud->points[i].z;
                } else {
                    ++nan;
                }
            }
            x.resize(size-nan);
            y.resize(size-nan);
            z.resize(size-nan);
        };
        void find_centroid (){
            cx = std::accumulate(x.begin(), x.end(), 0.0)/x.size();
            cy = std::accumulate(y.begin(), y.end(), 0.0)/y.size();
            cz = std::accumulate(z.begin(), z.end(), 0.0)/z.size();
        };
        void find_max(){
            max.x_max = *std::max_element(x.begin(), x.end());
            max.x_min = *std::min_element(x.begin(), x.end());
            max.y_max = *std::max_element(y.begin(), y.end());
            max.y_min = *std::min_element(y.begin(), y.end());
            max.z_max = *std::max_element(z.begin(), z.end());
            max.z_min = *std::min_element(z.begin(), z.end());
            max.xt =(max.x_max>std::fabs(max.x_min))? max.x_max : std::fabs(max.x_min);
            max.yt =(max.y_max>std::fabs(max.y_min))? max.y_max : std::fabs(max.y_min);
            max.zt =(max.z_max>std::fabs(max.z_min))? max.z_max : std::fabs(max.z_min);
            max.tt = (max.xt<max.yt)? (max.yt<max.zt)? max.zt:max.yt :(max.xt<max.zt)? max.zt:max.xt;
            x_len = std::fabs(max.x_max-max.x_min);
            y_len = std::fabs(max.y_max-max.y_min);
            z_len = std::fabs(max.z_max-max.z_min);
        };
        void move_centroid_to_origion (typename pcl::PointCloud<T>::Ptr input){
            for (size_t i=0;i<size;++i){
//                std::cout<<input->points[i].x<<" "<<input->points[i].y<<" "<<input->points[i].z<<" ";
                input->points[i].x -= cx;
                input->points[i].y -= cy;
                input->points[i].z -= cz;
//                std::cout<<input->points[i].x<<" "<<input->points[i].y<<" "<<input->points[i].z<<std::endl;
            }
        }
        
        void show(){
            printf("[max.xt, max.yt, max.zt, max.tt]: [%5.3f, %5.3f, %5.3f, %5.3f]\n", max.xt, max.yt,max.zt,max.tt);
            printf("[max.x_max, max.x_min, max.y_max, max.y_min, max.z_max, max.z_min]: [%5.3f, %5.3f, %5.3f, %5.3f, %5.3f, %5.3f]\n",max.x_max, max.x_min, max.y_max, max.y_min, max.z_max,max.z_min);
            printf("[cx, cy, cz]: [%5.3f, %5.3f, %5.3f]\n", cx, cy, cz);
            std::cout<<"size: "<<size<<std::endl;
        }
        
    };
    
    template <typename T>
    void VitruvianManifold (typename pcl::PointCloud<T>::Ptr cloud){
        if (typeid(T) == typeid(pcl::PointXYZRGBA) || typeid(T) == typeid(pcl::PointXYZRGB) ){
            //Calculate centroid and max value
            centroid<T> cmodel;
            cmodel.initialise(cloud);
//            cmodel.find_centroid();
            cmodel.find_max();
//            cmodel.show();
            //
            for (size_t i=0;i<cmodel.size;++i){
                cloud->points[i].r = std::fabs(cloud->points[i].x-cmodel.max.x_min)/cmodel.x_len*255;
                cloud->points[i].g = std::fabs(cloud->points[i].y-cmodel.max.y_min)/cmodel.y_len*255;
                cloud->points[i].b = std::fabs(cloud->points[i].z-cmodel.max.z_min)/cmodel.z_len*255;
                cloud->points[i].a = 255;
            }
            
        } else {
            std::cout<<"Doesn't support input type: "<< typeid(T).name()<<std::endl;
        }
    }
    
    template <typename T>
    void pointwisecolorhandler(typename pcl::PointCloud<T>::Ptr cloud, int R, int G, int B, int A){
        if (typeid(T) == typeid(pcl::PointXYZRGBA)){
            for (size_t i=0;i<cloud->size();++i){
                cloud->points[i].r = R;
                cloud->points[i].g = G;
                cloud->points[i].b = B;
                cloud->points[i].a = A;
            }
        } else if (typeid(T) == typeid(pcl::PointXYZRGB) ) {
            for (size_t i=0;i<cloud->size();++i){
                cloud->points[i].r = R;
                cloud->points[i].g = G;
                cloud->points[i].b = B;
            }
        } else {
            std::cout<<"Doesn't support input type: "<< typeid(T).name()<<std::endl;
        }
    }
    
    template <typename T>
    void pointwisecolorhandler(typename pcl::PointCloud<T>::Ptr cloud, int all, int A){
        if (typeid(T) == typeid(pcl::PointXYZRGBA)){
            for (size_t i=0;i<cloud->size();++i){
                cloud->points[i].r = all;
                cloud->points[i].g = all;
                cloud->points[i].b = all;
                cloud->points[i].a = A;
            }
        } else if (typeid(T) == typeid(pcl::PointXYZRGB) ) {
            for (size_t i=0;i<cloud->size();++i){
                cloud->points[i].r = all;
                cloud->points[i].g = all;
                cloud->points[i].b = all;
            }
        } else {
            std::cout<<"Doesn't support input type: "<< typeid(T).name()<<std::endl;
        }
    }
    
    /*!
     * @discussion Copy the color info from first input to the second
     */
    template <typename T>
    void pointwisecolorhandler(typename pcl::PointCloud<T>::Ptr cloud1, typename pcl::PointCloud<T>::Ptr cloud2){
        assert(cloud1->size() == cloud2->size() && "The size of two clouds must be equal");
        if (typeid(T) == typeid(pcl::PointXYZRGBA)){
            for (size_t i=0;i<cloud1->size();++i){
                cloud2->points[i].rgba = cloud1->points[i].rgba;
            }
        } else if (typeid(T) == typeid(pcl::PointXYZRGB) ) {
            for (size_t i=0;i<cloud1->size();++i){
                cloud2->points[i].rgb = cloud1->points[i].rgb;
            }
        } else {
            std::cout<<"Doesn't support input type: "<< typeid(T).name()<<std::endl;
        }
    }
    
    
    
    /** Euclidean Cluster Extraction */
    template <typename T>
    void euclidean_cluster_extraction (typename pcl::PointCloud<T>::Ptr cloud, std::vector<pcl::PointIndices>& cluster_indices){
        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
        tree->setInputCloud (cloud);

        pcl::EuclideanClusterExtraction<T> ec;
        ec.setClusterTolerance (0.2); // 2cm
        ec.setMinClusterSize (100);
        ec.setMaxClusterSize (25000);
        ec.setSearchMethod (tree);
        ec.setInputCloud (cloud);
        ec.extract (cluster_indices);
        
        pcl::PCDWriter writer;
        int j = 0;
        for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
        {
            pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
            for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
                cloud_cluster->points.push_back (cloud->points[*pit]); //*
            cloud_cluster->width = cloud_cluster->points.size ();
            cloud_cluster->height = 1;
            cloud_cluster->is_dense = true;
            
            std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;
            std::stringstream ss;
            ss << "cloud_cluster_" << j << ".pcd";
            writer.write<T> (ss.str (), *cloud_cluster, false); //*
            j++;
        }
    }
    
    /** KMean Clustering */
    template <typename T>
    void kmean_clustering (typename pcl::PointCloud<T>::Ptr cloud, int cluster_size, std::vector<pcl::PointCloud<T> >& output) {
        pcl::Kmeans kmeans(cloud->size(), 3);
        kmeans.setClusterSize(cluster_size);
        for (size_t i = 0; i < cloud->points.size(); i++)
        {
            std::vector<float> data(3);
            data[0] = cloud->points[i].x;
            data[1] = cloud->points[i].y;
            data[2] = cloud->points[i].z;
            kmeans.addDataPoint(data);
        }
        kmeans.kMeans();
        kmeans.kMeans();
        kmeans.kMeans();
        output.resize(cluster_size);
        std::vector<std::vector<float> > cen = kmeans.get_centroids();
        //assign point to nearest centroid
        for (int p=0;p<cloud->size();++p){
            float x = cloud->points[p].x;
            float y = cloud->points[p].y;
            float z = cloud->points[p].z;
            int eu_dis=0, eu_dis_pre=0, clust_num=0;
            for (int i=0;i<cen.size();++i){
                float cx = cen[i][0];
                float cy = cen[i][1];
                float cz = cen[i][2];
                //Euclidean distance
                eu_dis_pre = eu_dis;
                eu_dis = (cx-x)*(cx-x)+(cy-y)*(cy-y)+(cz-z)*(cz-z);
                (eu_dis > eu_dis_pre)? clust_num = i : eu_dis = eu_dis_pre;
            }
            T pt (x, y, z);
            output[clust_num].push_back(pt);
        }
    }
    
}



#endif /* PCL_tools_hpp */
